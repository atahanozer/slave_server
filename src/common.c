#include "common.h"

void destroySocket (int sockfd) {
	shutdown (sockfd, 0);
	shutdown (sockfd, 1);
	close (sockfd);
}

void recvn (int sockfd, void * buffer, int bufSize, int expectedDataSize) {
	int rc = -1;
	int cumulativeByteCount = 0;

	if (expectedDataSize > bufSize) {
		printf ("recvn - internal software error => bufSize : %d  -  expectedDataSize : %d\n", bufSize, expectedDataSize);
		destroySocket (sockfd);
		exit (0);
	}

	while (cumulativeByteCount < expectedDataSize) {
		rc = recv (sockfd, ((char *)buffer + cumulativeByteCount), bufSize - cumulativeByteCount, 0);
		if (rc < 0) {
			printf ("recvn - recv failed : %s\n", strerror (errno));
			destroySocket (sockfd);
			exit (0);
		}
		cumulativeByteCount += rc;
	}
}
