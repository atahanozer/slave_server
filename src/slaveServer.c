/*
 ============================================================================
 Name        : SLAVE_SERVER.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "common.h"

pthread_t udpKeepAliveSenderTid;

int udpMasterSockFd = -1, udpServerSockFd = -1;
struct sockaddr_in udpMasterAddrDescr;

int tcpSlaveServerSock = -1;
short int tcpServerPortNo = TCP_SLAVE_SERVER_BASE_PORT;
short int udpServerPortNo = UDP_SLAVE_SERVER_BASE_PORT;

void * udpKeepAliveSenderTask (void * ARGS) {
	timevar beforeRecv, afterRecv;
	long int timeDelta;
	int length, rc, numberOfTimeouts = 0;
	char message [IO_BUFFER_SIZE + 1] = {0};
	char answer  [IO_BUFFER_SIZE + 1] = {0};

	snprintf (message, IO_BUFFER_SIZE, "%s %06d %06d", KEEP_ALIVE_STR, tcpServerPortNo, udpServerPortNo);
	length = strlen (message);
	while (TRUE) {
		rc = sendto (udpMasterSockFd, message, length, 0, (struct sockaddr *) &udpMasterAddrDescr, sizeof (udpMasterAddrDescr));
		if (rc != length) {
			printf ("udpKeepAliveSenderTask - sendto failed : %s\n", strerror (errno));
			exit (1);
		}

		getCurrentTime (beforeRecv);
		rc = recv (udpServerSockFd, &answer, IO_BUFFER_SIZE, 0);//usleep (UDP_CLIENT_PERIOD);
		if (rc < 0) {
			if (errno != EAGAIN && errno != EWOULDBLOCK) {
				printf ("udpKeepAliveSenderTask - recv failed : %s\n", strerror (errno));
				exit (1);
			} else {
				// Timeout occured
				numberOfTimeouts ++;
				if (numberOfTimeouts == 2) {
					printf ("Master Bridge is unreachable. Terminating\n");
					exit (1);
				}
				continue;
			}
		} else {
			answer [rc] = '\0';
			if (strncmp (answer, STOP_STR, CONT_STOP_STRLEN) == 0) {
				printf ("Terminating in response to Master Bridge's request.\n");
				exit (0);
			}
		}
		getCurrentTime (afterRecv);

		timeDelta = diffUsec (beforeRecv, afterRecv);

		usleep (UDP_CLIENT_PERIOD - timeDelta);
	}

	return NULL;
}

void destroyKeepAliveSocket () {
	destroySocket (udpMasterSockFd);
	destroySocket (udpServerSockFd);
}

void createKeepAliveSockets (char * ipAddrStr) {
	in_addr_t addr;
	struct sockaddr_in serverAddrDescriptor;

	int rc;
	struct timeval timeout;

	timeout.tv_sec  = 0;
	timeout.tv_usec = UDP_CLIENT_PERIOD;

	udpMasterSockFd = socket (AF_INET, SOCK_DGRAM, 0);
	if (udpMasterSockFd < 0) {
		printf ("createKeepAliveSockets - client socket failed : %s\n", strerror (errno));
		exit (0);
	}

	udpServerSockFd = socket (AF_INET, SOCK_DGRAM, 0);
	if (udpServerSockFd < 0) {
		printf ("createKeepAliveSockets - server socket failed : %s\n", strerror (errno));
		destroyKeepAliveSocket ();
		exit (0);
	}

	memset (&serverAddrDescriptor, 0, sizeof (serverAddrDescriptor));
	serverAddrDescriptor.sin_addr.s_addr = htonl (INADDR_ANY);
	serverAddrDescriptor.sin_family      = AF_INET;
	serverAddrDescriptor.sin_port        = htons (udpServerPortNo);

	addr = inet_addr (ipAddrStr);
	if (addr == INADDR_NONE) {
		printf ("createKeepAliveSockets - invalid ip address : %s\n", ipAddrStr);
		destroyKeepAliveSocket ();
		exit (0);
	}

	while (bind (udpServerSockFd, (struct sockaddr *)&serverAddrDescriptor, sizeof (serverAddrDescriptor)) < 0) {
		if (errno == EADDRINUSE) {
			udpServerPortNo ++;
			if (udpServerPortNo == (UDP_SLAVE_SERVER_BASE_PORT + MAX_NUMBER_OF_SLAVE_SERVERS)) {
				printf ("createKeepAliveSockets - (REACHED MAXIMUM) bind failed : %s\n", strerror (errno));
				destroyKeepAliveSocket ();
				exit (0);
			}
			serverAddrDescriptor.sin_port = htons (udpServerPortNo);
		} else {
			printf ("createKeepAliveSockets - bind failed : %s\n", strerror (errno));
			destroyKeepAliveSocket ();
			exit (0);
		}
	}

	rc = setsockopt (udpServerSockFd, SOL_SOCKET, SO_RCVTIMEO, (void *)&timeout, sizeof(timeout));
	if (rc < 0) {
		printf ("createKeepAliveSockets - setsockopt failed : %s\n", strerror (errno));
		destroyKeepAliveSocket ();
		exit (0);
	}

	memset (&udpMasterAddrDescr, 0 , sizeof (udpMasterAddrDescr));
	udpMasterAddrDescr.sin_addr.s_addr = addr;
	udpMasterAddrDescr.sin_family      = AF_INET;
	udpMasterAddrDescr.sin_port        = htons (UDP_MASTER_LISTENER_PORT);

	atexit (&destroyKeepAliveSocket);
}

void destroyTcpServerSocket () {
	destroySocket (tcpSlaveServerSock);
}

void initiateTcpServerSocket () {
	struct sockaddr_in serverAddrDesc;
	int rc;

	tcpSlaveServerSock = socket (AF_INET, SOCK_STREAM, 0);

	if (tcpSlaveServerSock < 0) {
		printf ("initiateTcpServerSocket - socket failed : %s\n", strerror (errno));
		exit (0);
	}

	memset (&serverAddrDesc, 0, sizeof (serverAddrDesc));
	serverAddrDesc.sin_family      = AF_INET;
	serverAddrDesc.sin_addr.s_addr = htonl (INADDR_ANY);
	serverAddrDesc.sin_port        = htons (tcpServerPortNo);

	while ((bind (tcpSlaveServerSock, (struct sockaddr *) &serverAddrDesc, sizeof (serverAddrDesc)) < 0)) {
		if (errno == EADDRINUSE) {
			tcpServerPortNo ++;
			if (tcpServerPortNo == (TCP_SLAVE_SERVER_BASE_PORT + MAX_NUMBER_OF_SLAVE_SERVERS)) {
				printf ("initiateTcpServerSocket - (REACHED MAXIMUM) bind failed : %s\n", strerror (errno));
				destroyTcpServerSocket ();
				exit (0);
			}
			serverAddrDesc.sin_port = htons (tcpServerPortNo);
		} else {
			printf ("initiateTcpServerSocket - bind failed : %s\n", strerror (errno));
			destroyTcpServerSocket ();
			exit (0);
		}
	}

	rc = listen (tcpSlaveServerSock, MAX_TCP_CLIENTS);

	if (rc < 0) {
		printf ("initiateTcpServerSocket - accept failed : %s\n", strerror (errno));
		destroyTcpServerSocket ();
		exit (0);
	}

	atexit (destroyTcpServerSocket);
}

int main(int argc, char * argv[]) {
	transaction_t transaction;
	int rc, clientFd;
	struct sockaddr_in clientAddrDesc;
	socklen_t clientAddrDescLen = sizeof (clientAddrDesc);

	if (argc != 2) {
		printf ("Usage : %s <ip_address_of_master_server>\n", argv [0]);
		exit (0);
	}

	initiateTcpServerSocket ();

	createKeepAliveSockets (argv [1]);

	rc = pthread_create (&udpKeepAliveSenderTid, NULL, udpKeepAliveSenderTask, NULL);

	if (rc < 0) {
		printf ("main - pthread_create failed : %s\n", strerror (errno));
		exit (0);
	}

	pthread_detach (udpKeepAliveSenderTid);

	clientFd = accept (tcpSlaveServerSock, (struct sockaddr *) &clientAddrDesc, &clientAddrDescLen);
	if (clientFd < 0) {
		printf ("main - accept failed : %s\n", strerror (errno));
	}

	while (TRUE) {
		recvn (clientFd, &transaction, sizeof (transaction_t), sizeof (transaction_t));

		if (transaction.operation != -1) {
			/*
			HANDLE INCOMING TRANSACTIONS
			*/
			printf ("\n-------------------------------------------\n");
			printf ("A new transaction has been received\n");
			printf ("transaction.srcCustomerId  : %d\n", transaction.srcCustomerId);
			printf ("transaction.srcBranchId    : %d\n", transaction.srcBranchId);
			printf ("transaction.destCustomerId : %d\n", transaction.destCustomerId);
			printf ("transaction.destBranchId   : %d\n", transaction.destBranchId);
			printf ("transaction.operation      : %d\n", transaction.operation);
			printf ("-------------------------------------------\n");
		} else {
			printf ("\nEnd Of Transactions\n");
			break;
		}
	}

	destroySocket (clientFd);

	return 0;
}
